FROM golang:1-alpine as build

WORKDIR /build
COPY src src
WORKDIR /build/src
RUN go build cmd/hello/hello.go

FROM alpine:latest

WORKDIR /app
COPY --from=build /build/src/hello /app/hello

EXPOSE 8000
ENTRYPOINT ["./hello"]
