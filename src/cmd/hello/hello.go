package main

import (
	"hello-go/pkg/http"
)

func main() {
	addr := ":8000"
	http.StartSvc(addr)
}
