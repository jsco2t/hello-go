package domain

import (
	uuid "github.com/google/uuid"
)

type Response struct {
	ID      uuid.UUID
	Message string
}

type HelloSvc interface {
	CreateResponse(requestPath string) (*Response, error)
	StartSvc(address string)
}
