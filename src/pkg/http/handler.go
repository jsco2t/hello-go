package http

import (
	"fmt"
	"github.com/google/uuid"
	"hello-go/pkg/domain"
	"log"
	"net/http"
)

func CreateResponse(requestPath string) (*domain.Response, error) {

	response := domain.Response{
		ID:      uuid.New(),
		Message: "",
	}

	response.Message = fmt.Sprintf("Hello Go, you requested: %s\n", requestPath)

	return &response, nil
}

func Handler(response http.ResponseWriter, request *http.Request) {
	responseMessage, _ := CreateResponse(request.URL.Path)
	_, err := fmt.Fprintf(response, "[%s] [%s]", responseMessage.ID, responseMessage.Message)

	if err != nil {
		log.Fatalf("Failed creating response message: %v", err)
	}

	log.Printf("[%s] Received request for: %s", responseMessage.ID, request.URL.Path)
}

func StartSvc(address string) {
	handler := http.HandlerFunc(Handler)
	if err := http.ListenAndServe(address, handler); err != nil {
		log.Fatalf("Could not start server for: %s: %v", address, err)
	}
}
